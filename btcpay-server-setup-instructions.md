To get BTCPay server working on my Trisquel 9 VM, I basically followed the instructions from the link below with some slight modifications

https://docs.btcpayserver.org/ManualDeployment/#minimal-manual-setup

## INSTRUCTIONS AND MODIFICATIONS I MADE TO THE INSTRUCTIONS TO GET THE SERVER RUNNING:

1.) I installed Bitcoin Core and Dotnet Core on the machine.

You can get the shell script for doing this by cloning this repo.
The script is available in `./shell-scripts/bitcoin-dotnet-core.sh`

2.) I installed NBXplorer and BTCPay server.

The script for this is available in this repo in `./shell-scripts/nbxplorer-btcpay-server.sh`

3.) I ran Bitcoin Core in a separate terminal. I used the --prune option with the `bitciond` command to enable bitcoin core to delete already verified blocks and avoid taking up all the space on my VM like so:

`bitcoind --prune=550`

The script for this is available in `./shell-scripts/run-bitcoin.sh`

4.) In a separate terminal, I executed NBXplorer. The shell script I used for this is available in `./shell-scripts/run-nbxplorer.sh`

5.) In another separate terminal, I executed the BTCPay Server. The shell script I used for this is available in `./shell-scripts/run-btcpayserver.sh`
